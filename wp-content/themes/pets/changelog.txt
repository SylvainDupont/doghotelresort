Version 2.0
	* Revslider plugins updated
    * Theme compatibility with new version of RevSlider is improved.
    * Visual Composer is updated

Version 1.9
    * WordPress 4.5 ready
    * All plugins updated
    * Layout of shortcode [trx_gap] is fixed.

Version 1.8.1
	Visual Composer is updated to its latest version

Version 1.8
    + Compatibility with VC latest versions is improved.
    + Instagram plugin updated
    Fixed: Layout of shortcode [trx_gap] is fixed.

Version 1.7
	* Theme compatibility with new version of Visual Composer 4.6.0 is improved.
	+ Plugins included in the theme, updated to their latest versions.

Version 1.6
	 * Fixed: Script Google maps
     * Fixed: 'Google map' shortcode is updated
	 * Fixed: Update amount on the cart button

Version 1.5
	* Fixed: Registration form is corrected.
	* Fixed: Contact information output in user-panel is corrected.

Version 1.4
	+ Added: Child theme
	* Dummy data installation improved

Version 1.3
	+ Added: Compatibility with BuddyPress and bbPress plug-ins with basic styles is added.
	+ Added: An option to center menu to the entire width is added (compared to the menu output only to the right from header)

Version 1.2
	* Fixed: We have slightly changed and fixed demo contents.

Version 1.1
	+ Added: Built-in demo data import: one click  - and your website becomes a precise copy of our demo site!

Version 1.0
	Release