<?php
/*
 * The template for displaying "Page 404"
*/
?>
<section class="post no_margin">
	<article class="post_content">
		<div class="page404">
			<div class="titleError"><span><?php _e('Oops!', 'themerex'); ?></span><?php _e( '404', 'themerex' ); ?></div>
			<div class="h4 sc_title_underline"><?php _e('Désolé ! Nous n\'avons pas trouvé la page demandée', 'themerex'); ?></div>
			<p><?php echo sprintf(__('Vous pouvez également revenir à <a href="%s">%s</a>', 'themerex'), esc_url(home_url()), get_bloginfo()); ?>
			<br><?php _e('et commencer à naviguer à partir de cette page.', 'themerex'); ?></p>
		</div>
	</article>
</section>