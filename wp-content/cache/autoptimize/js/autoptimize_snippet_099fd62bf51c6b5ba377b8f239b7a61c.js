if(typeof(tmt)=="undefined"){alert("Error: tmt.core JavaScript library missing");}
tmt.form={};tmt.form.MESSAGE_CLASS="tmtFormMessage";tmt.form.ERROR_MESSAGE_CLASS="tmtFormErrorMessage";tmt.form.checkFields=function(){tmt.setNodeAttribute(arguments,"checked",true);}
tmt.form.uncheckFields=function(){tmt.setNodeAttribute(arguments,"checked",false);}
tmt.form.toggleCheckFields=function(){for(var i=0;i<arguments.length;i++){var fieldNode=tmt.get(arguments[i]);if(fieldNode){fieldNode.checked?fieldNode.checked=false:fieldNode.checked=true;}}}
tmt.form.checkSelect=function(theNode,values){var valueArray=values.split(",");var selectNode=tmt.get(theNode);if(selectNode==null){selectNode=document.getElementsByName(theNode)[0];}
for(var i=0;i<selectNode.options.length;i++){for(var j=0;j<valueArray.length;j++){if(valueArray[j]==tmt.form.getOptionNodeValue(selectNode.options[i])){selectNode.options[i].selected=true;}}}}
tmt.form.resetSelect=function(theNode){var selectNode=tmt.get(theNode);if(selectNode==null){selectNode=document.getElementsByName(theNode)[0];}
for(var i=0;i<selectNode.options.length;i++){selectNode.options[i].selected=false;}}
tmt.form.selectAll=function(theNode){var selectNode=tmt.get(theNode);if(selectNode==null){selectNode=document.getElementsByName(theNode)[0];}
var fieldType=selectNode.type.toLowerCase();if(fieldType=="select-multiple"){for(var i=0;i<selectNode.options.length;i++){selectNode.options[i].selected=true;}}}
tmt.form.checkGroup=function(groupName,values){var valueArray=values.split(",");var groupNodes=document.getElementsByName(groupName);for(var i=0;i<groupNodes.length;i++){for(var j=0;j<valueArray.length;j++){if(groupNodes[i].value==valueArray[j]){groupNodes[i].checked=true;}}}}
tmt.form.resetGroup=function(groupName){var groupNodes=document.getElementsByName(groupName);for(var i=0;i<groupNodes.length;i++){groupNodes[i].checked=false;}}
tmt.form.disableFields=function(){tmt.setNodeAttribute(arguments,"disabled",true);}
tmt.form.enableFields=function(){tmt.setNodeAttribute(arguments,"disabled",false);}
tmt.form.toggleEnableFields=function(){for(var i=0;i<arguments.length;i++){var fieldNode=tmt.get(arguments[i]);if(fieldNode){fieldNode.disabled?fieldNode.disabled=false:fieldNode.disabled=true;}}}
tmt.form.getParentForm=function(startNode){var parentObj=startNode.parentNode;while(parentObj){if(parentObj.tagName.toLowerCase()=="body"){return false;}
if(parentObj.tagName.toLowerCase()=="form"){return parentObj;}
else{parentObj=parentObj.parentNode;continue;}}
return false;}
tmt.form.getOptionNodeValue=function(optionNode){if(window.ActiveXObject){if(optionNode.attributes["value"].specified){return optionNode.value;}}
else{if(optionNode.hasAttribute("value")){return optionNode.value;}}
return optionNode.text;}
tmt.form.isFormField=function(fieldNode){if(!fieldNode.type){return false;}
if((fieldNode.type.toLowerCase()=="fieldset")||(fieldNode.type.toLowerCase()=="reset")||(fieldNode.type.toLowerCase()=="button")){return false;}
return true}
tmt.form.getChildFields=function(startNode){var childFields=[];var childNodes=tmt.getAllNodes(startNode);for(var i=0;i<childNodes.length;i++){if(tmt.form.isFormField(childNodes[i])){childFields.push(childNodes[i]);}}
return childFields;}
tmt.form.getSubmitNodes=function(startNode){var inputNodes=startNode.getElementsByTagName("input");return tmt.filterNodesByAttributeValue("type","submit",inputNodes);}
tmt.form.getFieldGroup=function(fieldNode){var boxes=[];if(fieldNode.name){boxes=tmt.getNodesByAttributeValue("name",fieldNode.name,fieldNode.form);}
return boxes;}
tmt.form.getValue=function(field,getGroupValue){var retValue="";var fieldNode=tmt.get(field);var fieldType=fieldNode.type.toLowerCase();switch(fieldType){case"select-multiple":for(var j=0;j<fieldNode.options.length;j++){if(fieldNode.options[j].selected){if(retValue==""){retValue=tmt.form.getOptionNodeValue(fieldNode.options[j]);}
else{retValue+=",";retValue+=tmt.form.getOptionNodeValue(fieldNode.options[j]);}}}
break;case"select-one":for(var k=0;k<fieldNode.options.length;k++){if(fieldNode.options[k].selected){retValue=tmt.form.getOptionNodeValue(fieldNode.options[k])
break;}}
break;case"radio":case"checkbox":if(!getGroupValue||!fieldNode.name){if(fieldNode.checked){retValue=fieldNode.value;}}
else{var boxes=tmt.form.getFieldGroup(fieldNode);retValue=tmt.form.getGroupValue(boxes);}
break;case"reset":break;case"button":break;default:if(window.ActiveXObject&&fieldNode.id&&(typeof(tinyMCE)!="undefined")&&tinyMCE.get(fieldNode.id)){retValue=tinyMCE.get(fieldNode.id).getContent();}
else{retValue=fieldNode.value;}
break;}
return retValue;}
tmt.form.getGroupValue=function(boxes){var values=[];for(var i=0;i<boxes.length;i++){if(boxes[i].checked){values.push(boxes[i].value);}}
return values.toString();}
tmt.form.hashForm=function(formNode,demoronize){var valueObj={};for(var i=0;i<formNode.elements.length;i++){var fieldNode=formNode.elements[i];if(!fieldNode||!fieldNode.name||fieldNode.tagName.toLowerCase()=="fieldset"){continue;}
var fieldName=fieldNode.name;valueObj[fieldName]=tmt.form.getValue(fieldNode,true);}
return valueObj;}
tmt.form.serializeForm=function(formNode,demoronize){return tmt.hashToEncodeURI(tmt.form.hashForm(formNode,demoronize));}
tmt.form.clearForm=function(formNode){tmt.form.clearFields(formNode.elements);}
tmt.form.clearFields=function(fieldNodes){for(var i=0;i<fieldNodes.length;i++){tmt.form.clearField(fieldNodes[i]);}}
tmt.form.clearField=function(fieldNode){if(!fieldNode||fieldNode.tagName.toLowerCase()=="fieldset"){return;}
var fieldType=fieldNode.type.toLowerCase();switch(fieldType){case"select-multiple":case"select-one":fieldNode.selectedIndex=-1;break;case"radio":case"checkbox":fieldNode.checked=false;break;case"reset":break;case"button":break;default:fieldNode.value="";break;}}
tmt.form.MSG_BOX_ID="tmtFormMessageBox";tmt.form.generateBoxId=function(formNode){var errorId=tmt.form.MSG_BOX_ID
if(formNode.getAttribute("id")){errorId+=formNode.getAttribute("id");}
else if(formNode.getAttribute("name")){errorId+=formNode.getAttribute("name");}
return errorId;}
tmt.form.displayMessage=function(formNode,html){tmt.form.displayBox(formNode,html,tmt.form.MESSAGE_CLASS);}
tmt.form.displayErrorMessage=function(formNode,html){tmt.form.displayBox(formNode,html,tmt.form.ERROR_MESSAGE_CLASS);}
tmt.form.displayBox=function(formNode,html,cssClass){if(!cssClass){cssClass=tmt.form.MESSAGE_CLASS;}
var displayNode=document.createElement("div");var errorId=tmt.form.generateBoxId(formNode);displayNode.setAttribute("id",errorId);displayNode.className=cssClass;displayNode.innerHTML=html;var oldDisplay=tmt.get(errorId);if(oldDisplay){formNode.parentNode.replaceChild(displayNode,oldDisplay);}
else{formNode.parentNode.insertBefore(displayNode,formNode);}}
tmt.form.removeDisplayBox=function(formNode){var errorId=tmt.form.generateBoxId(formNode);var oldDisplay=tmt.get(errorId);if(oldDisplay){oldDisplay.parentNode.removeChild(oldDisplay);}}
tmt.form.stringDemoronizer=function(str){str=str.replace(new RegExp(String.fromCharCode(710),"g"),"^");str=str.replace(new RegExp(String.fromCharCode(732),"g"),"~");str=str.replace(new RegExp(String.fromCharCode(8216),"g"),"'");str=str.replace(new RegExp(String.fromCharCode(8217),"g"),"'");str=str.replace(new RegExp(String.fromCharCode(8220),"g"),'"');str=str.replace(new RegExp(String.fromCharCode(8221),"g"),'"');str=str.replace(new RegExp(String.fromCharCode(8211),"g"),"-");str=str.replace(new RegExp(String.fromCharCode(8212),"g"),"--");str=str.replace(new RegExp(String.fromCharCode(8218),"g"),",");str=str.replace(new RegExp(String.fromCharCode(8222),"g"),",,");str=str.replace(new RegExp(String.fromCharCode(8226),"g"),"*");str=str.replace(new RegExp(String.fromCharCode(8230),"g"),"...");str=str.replace(new RegExp(String.fromCharCode(8364),"g"),"�");return str;};