if(typeof(tmt)=="undefined"){alert("Error: tmt.core JavaScript library missing");}
if(typeof(tmt.form)=="undefined"){alert("Error: tmt.form JavaScript library missing");}
tmt.validator={};tmt.validator.DEFAULT_DATE_PATTERN="YYYY-MM-DD";tmt.validator.DEFAULT_CALLBACK="tmt.validator.defaultCallback";tmt.validator.DEFAULT_CALLBACK_MULTISECTION="tmt.validator.multiSectionDefaultCallback";tmt.validator.init=function(){var formNodes=tmt.filterNodesByAttributeValue("tmt:validate","true",document.getElementsByTagName("form"));for(var i=0;i<formNodes.length;i++){formNodes[i].tmt_validator=true;tmt.validator.filters.init(formNodes[i].elements);if(typeof formNodes[i].onsubmit!="function"){formNodes[i].onsubmit=function(){return tmt.validator.validateForm(this);}}
else{formNodes[i].tmt_oldSubmit=formNodes[i].onsubmit;formNodes[i].onsubmit=function(){if(this.tmt_oldSubmit()){return tmt.validator.validateForm(this);}
return false;}}}}
tmt.validator.validateForm=function(form){var formNode=tmt.get(form);formNode.tmt_validator=true;var formValidator=tmt.validator.formValidatorFactory(formNode);var activeValidators=tmt.validator.executeValidators(formValidator.validators);eval(formValidator.callback+"(formNode, activeValidators)");if(activeValidators.length==0){formValidator.blockSubmit();}
return activeValidators.length==0;}
tmt.validator.validateFields=function(fieldsArray,callback){if(fieldsArray.length==0){return true;}
if(!callback){callback=tmt.validator.getCallback(tmt.get(fieldsArray[0]).form);}
var formNode=tmt.get(fieldsArray[0]).form;var validators=[];for(var i=0;i<fieldsArray.length;i++){var fieldNode=tmt.get(fieldsArray[i]);if(tmt.form.isFormField(fieldNode)){validators.push(tmt.validator.fieldValidatorFactory(fieldNode));}}
var activeValidators=tmt.validator.executeValidators(validators);eval(callback+"(formNode, activeValidators)");return activeValidators.length==0;}
tmt.validator.validateChildFields=function(startNode,callback){var fieldsArray=tmt.form.getChildFields(startNode);return tmt.validator.validateFields(fieldsArray,callback);}
tmt.validator.validateField=function(field,callback){var fieldNode=tmt.get(field);if(!tmt.form.isFormField(fieldNode)){return false;}
if(!callback){callback="tmt.validator.defaultFieldCallback";}
var fieldType=fieldNode.type.toLowerCase();if(fieldNode.tagName.toLowerCase()=="fieldset"){return;}
var validator=tmt.validator.fieldValidatorFactory(fieldNode);var haveError=validator.validate();if(haveError){eval(callback+"(fieldNode, validator)");}
else{eval(callback+"(fieldNode, null)");}
return haveError;}
tmt.validator.executeValidators=function(validators){var validatedFields={};var activeValidators=[];for(var i=0;i<validators.length;i++){if(validators[i].validate){if(validatedFields[validators[i].name]){continue;}
if(validators[i].validate()){activeValidators[activeValidators.length]=validators[i];}
validatedFields[validators[i].name]=true;}}
return activeValidators;}
tmt.validator.formValidatorFactory=function(formNode){var obj={};obj.validators=[];obj.callback=tmt.validator.getCallback(formNode);for(var i=0;i<formNode.elements.length;i++){if(tmt.form.isFormField(formNode.elements[i])){obj.validators.push(tmt.validator.fieldValidatorFactory(formNode.elements[i]));}}
obj.buttons=tmt.form.getSubmitNodes(formNode);obj.blockSubmit=function(){if(!formNode.getAttribute("tmt:blocksubmit")&&!(formNode.getAttribute("tmt:blocksubmit")=="false")){for(var i=0;i<obj.buttons.length;i++){if(obj.buttons[i].getAttribute("tmt:waitmessage")){obj.buttons[i].value=obj.buttons[i].getAttribute("tmt:waitmessage");}
obj.buttons[i].disabled=true;}}}
return obj;}
tmt.validator.fieldValidatorFactory=function(fieldNode){var fieldType=fieldNode.type.toLowerCase();var validator={};if(fieldNode.tagName.toLowerCase()=="fieldset"){return validator;}
switch(fieldType){case"select-multiple":validator=tmt.validator.selectValidatorFactory(fieldNode);break;case"select-one":validator=tmt.validator.selectValidatorFactory(fieldNode);break;case"radio":validator=tmt.validator.radioValidatorFactory(tmt.form.getFieldGroup(fieldNode));break;case"checkbox":validator=tmt.validator.boxValidatorFactory(tmt.form.getFieldGroup(fieldNode));break;case"reset":return validator;break;case"button":return validator;break;default:validator=tmt.validator.textValidatorFactory(fieldNode);break;}
return validator;}
tmt.validator.abstractValidatorFactory=function(fieldNode){var obj={};obj.message="";obj.name="";if(fieldNode.name){obj.name=fieldNode.name;}
else if(fieldNode.id){obj.name=fieldNode.id;}
obj.errorClass="";if(fieldNode.getAttribute("tmt:message")){obj.message=fieldNode.getAttribute("tmt:message");}
if(fieldNode.getAttribute("tmt:errorclass")){obj.errorClass=fieldNode.getAttribute("tmt:errorclass");}
obj.flagInvalid=function(){if(obj.errorClass){tmt.addClass(fieldNode,obj.errorClass);}
fieldNode.setAttribute("title",obj.message);}
obj.flagValid=function(){if(obj.errorClass){tmt.removeClass(fieldNode,obj.errorClass);}
fieldNode.removeAttribute("title");}
obj.validate=function(){if(fieldNode.disabled){obj.flagValid();return false;}
if(!obj.isValid()){obj.flagInvalid();return true;}
else{obj.flagValid();return false;}}
return obj;}
tmt.validator.textValidatorFactory=function(fieldNode){var obj=tmt.validator.abstractValidatorFactory(fieldNode);obj.type="text";obj.getFocus=function(){try{fieldNode.focus();fieldNode.select();}
catch(exception){}}
obj.isEmpty=function(){return fieldNode.value=="";}
obj.isRequired=function(){var requiredAtt=fieldNode.getAttribute("tmt:required");if(requiredAtt){if((requiredAtt=="true")||(requiredAtt=="false")){return eval(requiredAtt);}
return(eval(requiredAtt+"(fieldNode)"));}
return false;}
obj.isValid=function(){if(obj.isEmpty()){if(obj.isRequired()){return false;}
else{return true;}}
else{for(var rule in tmt.validator.rules){if(fieldNode.getAttribute("tmt:"+rule)){if(!eval("tmt.validator.rules."+rule+"(fieldNode)")){return false;}}}}
return true;}
return obj;}
tmt.validator.selectValidatorFactory=function(selectNode){var obj=tmt.validator.abstractValidatorFactory(selectNode);obj.type="select";var invalidIndex;if(selectNode.getAttribute("tmt:invalidindex")){invalidIndex=selectNode.getAttribute("tmt:invalidindex");}
var invalidValue;if(selectNode.getAttribute("tmt:invalidvalue")!=null){invalidValue=selectNode.getAttribute("tmt:invalidvalue");}
obj.isValid=function(){if(selectNode.selectedIndex==-1){selectNode.selectedIndex=0;}
if(selectNode.selectedIndex==invalidIndex){return false;}
if(tmt.form.getValue(selectNode)==invalidValue){return false;}
for(var rule in tmt.validator.rules){if(selectNode.getAttribute("tmt:"+rule)){if(!eval("tmt.validator.rules."+rule+"(selectNode)")){return false;}}}
return true;}
return obj;}
tmt.validator.groupValidatorFactory=function(buttonGroup){var obj={};obj.name=buttonGroup[0].name;obj.message="";obj.errorClass="";for(var i=0;i<buttonGroup.length;i++){if(buttonGroup[i].getAttribute("tmt:message")){obj.message=buttonGroup[i].getAttribute("tmt:message");}
if(buttonGroup[i].getAttribute("tmt:errorclass")){obj.errorClass=buttonGroup[i].getAttribute("tmt:errorclass");}}
obj.flagInvalid=function(){if(obj.errorClass){for(var i=0;i<buttonGroup.length;i++){tmt.addClass(buttonGroup[i],obj.errorClass);buttonGroup[i].setAttribute("title",obj.message);}}}
obj.flagValid=function(){if(obj.errorClass){for(var i=0;i<buttonGroup.length;i++){tmt.removeClass(buttonGroup[i],obj.errorClass);buttonGroup[i].removeAttribute("title");}}}
obj.validate=function(){if(obj.isValid()){obj.flagValid();return false;}
else{obj.flagInvalid();return true;}}
return obj;}
tmt.validator.boxValidatorFactory=function(boxGroup){var obj=tmt.validator.groupValidatorFactory(boxGroup);obj.type="checkbox";var minchecked=0;var maxchecked=boxGroup.length;for(var i=0;i<boxGroup.length;i++){if(boxGroup[i].getAttribute("tmt:minchecked")){minchecked=boxGroup[i].getAttribute("tmt:minchecked");}
if(boxGroup[i].getAttribute("tmt:maxchecked")){maxchecked=boxGroup[i].getAttribute("tmt:maxchecked");}}
obj.isValid=function(){var checkCounter=0;for(var i=0;i<boxGroup.length;i++){if(boxGroup[i].checked){checkCounter++;}}
return(checkCounter>=minchecked)&&(checkCounter<=maxchecked);}
return obj;}
tmt.validator.radioValidatorFactory=function(radioGroup){var obj=tmt.validator.groupValidatorFactory(radioGroup);obj.type="radio";obj.isRequired=function(){var requiredFlag=false;for(var i=0;i<radioGroup.length;i++){if(radioGroup[i].disabled==false){if(radioGroup[i].getAttribute("tmt:required")){requiredFlag=radioGroup[i].getAttribute("tmt:required");}}}
return requiredFlag;}
obj.isValid=function(){if(obj.isRequired()){for(var i=0;i<radioGroup.length;i++){if(radioGroup[i].checked){return true;}}
return false;}
else{return true;}}
return obj;}
tmt.validator.rules={};tmt.validator.rules.datepattern=function(fieldNode){var datObj=tmt.validator.dateStrToObj(fieldNode.value,fieldNode.getAttribute("tmt:datepattern"));if(datObj){return true;}
return false;}
tmt.validator.rules.maxdate=function(fieldNode){var pattern=tmt.validator.DEFAULT_DATE_PATTERN;if(fieldNode.getAttribute("tmt:datepattern")){pattern=fieldNode.getAttribute("tmt:datepattern");}
var valueDate=tmt.validator.dateStrToObj(fieldNode.value,pattern);var maxDate=tmt.validator.dateStrToObj(fieldNode.getAttribute("tmt:maxdate"),pattern);if(valueDate&&maxDate){return valueDate<=maxDate;}
return false;}
tmt.validator.rules.mindate=function(fieldNode){var pattern=tmt.validator.DEFAULT_DATE_PATTERN;if(fieldNode.getAttribute("tmt:datepattern")){pattern=fieldNode.getAttribute("tmt:datepattern");}
var valueDate=tmt.validator.dateStrToObj(fieldNode.value,pattern);var minDate=tmt.validator.dateStrToObj(fieldNode.getAttribute("tmt:mindate"),pattern);if(valueDate&&minDate){return valueDate>=minDate;}
return false;}
tmt.validator.rules.equalto=function(fieldNode){var twinNode=document.getElementById(fieldNode.getAttribute("tmt:equalto"));return twinNode.value==fieldNode.value;}
tmt.validator.rules.maxlength=function(fieldNode){if(fieldNode.value.length>fieldNode.getAttribute("tmt:maxlength")){return false;}
return true;}
tmt.validator.rules.maxnumber=function(fieldNode){if(parseFloat(fieldNode.value)>fieldNode.getAttribute("tmt:maxnumber")){return false;}
return true;}
tmt.validator.rules.minlength=function(fieldNode){if(fieldNode.value.length<fieldNode.getAttribute("tmt:minlength")){return false;}
return true;}
tmt.validator.rules.minnumber=function(fieldNode){if(parseFloat(fieldNode.value)<fieldNode.getAttribute("tmt:minnumber")){return false;}
return true;}
tmt.validator.rules.pattern=function(fieldNode){var reg=tmt.validator.patterns[fieldNode.getAttribute("tmt:pattern")];if(reg){return reg.test(fieldNode.value);}
else{return true;}}
tmt.validator.patterns={};tmt.validator.patterns.email=new RegExp("^[\\w\\.=-]+@[\\w\\.-]+\\.[\\w\\.-]{2,4}$");tmt.validator.patterns.lettersonly=new RegExp("^[a-zA-Z]*$");tmt.validator.patterns.alphanumeric=new RegExp("^\\w*$");tmt.validator.patterns.integer=new RegExp("^-?\\d\\d*$");tmt.validator.patterns.positiveinteger=new RegExp("^\\d\\d*$");tmt.validator.patterns.number=new RegExp("^-?(\\d\\d*\\.\\d*$)|(^-?\\d\\d*$)|(^-?\\.\\d\\d*$)");tmt.validator.patterns.filepath_pdf=new RegExp("[\\w_]*\\.([pP][dD][fF])$");tmt.validator.patterns.filepath_jpg_gif=new RegExp("[\\w_]*\\.([gG][iI][fF])|([jJ][pP][eE]?[gG])$");tmt.validator.patterns.filepath_jpg=new RegExp("[\\w_]*\\.([jJ][pP][eE]?[gG])$");tmt.validator.patterns.filepath_zip=new RegExp("[\\w_]*\\.([zZ][iI][pP])$");tmt.validator.patterns.filepath=new RegExp("[\\w_]*\\.\\w{3}$");tmt.validator.datePatterns={};tmt.validator.createDatePattern=function(rex,year,month,day,separator){var infoObj={};infoObj.rex=new RegExp(rex);infoObj.y=year;infoObj.m=month;infoObj.d=day;infoObj.s=separator;return infoObj;}
tmt.validator.datePatterns["YYYY-MM-DD"]=tmt.validator.createDatePattern("^\([0-9]{4}\)\\-\([0-1][0-9]\)\\-\([0-3][0-9]\)$",0,1,2,"-");tmt.validator.datePatterns["YYYY-M-D"]=tmt.validator.createDatePattern("^\([0-9]{4}\)\\-\([0-1]?[0-9]\)\\-\([0-3]?[0-9]\)$",0,1,2,"-");tmt.validator.datePatterns["MM.DD.YYYY"]=tmt.validator.createDatePattern("^\([0-1][0-9]\)\\.\([0-3][0-9]\)\\.\([0-9]{4}\)$",2,0,1,".");tmt.validator.datePatterns["M.D.YYYY"]=tmt.validator.createDatePattern("^\([0-1]?[0-9]\)\\.\([0-3]?[0-9]\)\\.\([0-9]{4}\)$",2,0,1,".");tmt.validator.datePatterns["MM/DD/YYYY"]=tmt.validator.createDatePattern("^\([0-1][0-9]\)\/\([0-3][0-9]\)\/\([0-9]{4}\)$",2,0,1,"/");tmt.validator.datePatterns["M/D/YYYY"]=tmt.validator.createDatePattern("^\([0-1]?[0-9]\)\/\([0-3]?[0-9]\)\/\([0-9]{4}\)$",2,0,1,"/");tmt.validator.datePatterns["MM-DD-YYYY"]=tmt.validator.createDatePattern("^\([0-21][0-9]\)\\-\([0-3][0-9]\)\\-\([0-9]{4}\)$",2,0,1,"-");tmt.validator.datePatterns["M-D-YYYY"]=tmt.validator.createDatePattern("^\([0-1]?[0-9]\)\\-\([0-3]?[0-9]\)\\-\([0-9]{4}\)$",2,0,1,"-");tmt.validator.datePatterns["DD.MM.YYYY"]=tmt.validator.createDatePattern("^\([0-3][0-9]\)\\.\([0-1][0-9]\)\\.\([0-9]{4}\)$",2,1,0,".");tmt.validator.datePatterns["D.M.YYYY"]=tmt.validator.createDatePattern("^\([0-3]?[0-9]\)\\.\([0-1]?[0-9]\)\\.\([0-9]{4}\)$",2,1,0,".");tmt.validator.datePatterns["DD/MM/YYYY"]=tmt.validator.createDatePattern("^\([0-3][0-9]\)\/\([0-1][0-9]\)\/\([0-9]{4}\)$",2,1,0,"/");tmt.validator.datePatterns["D/M/YYYY"]=tmt.validator.createDatePattern("^\([0-3]?[0-9]\)\/\([0-1]?[0-9]\)\/\([0-9]{4}\)$",2,1,0,"/");tmt.validator.datePatterns["DD-MM-YYYY"]=tmt.validator.createDatePattern("^\([0-3][0-9]\)\\-\([0-1][0-9]\)\\-\([0-9]{4}\)$",2,1,0,"-");tmt.validator.datePatterns["D-M-YYYY"]=tmt.validator.createDatePattern("^\([0-3]?[0-9]\)\\-\([0-1]?[0-9]\)\\-\([0-9]{4}\)$",2,1,0,"-");tmt.validator.filters={};tmt.validator.filters.init=function(fields){for(var i=0;i<fields.length;i++){if(fields[i].getAttribute("tmt:filters")){tmt.addEvent(fields[i],"keydown",function(){tmt.validator.filterField(this);});tmt.addEvent(fields[i],"blur",function(){tmt.validator.filterField(this);});}}}
tmt.validator.createFilter=function(rex,replaceStr){var infoObj={};infoObj.rex=new RegExp(rex,"g");infoObj.str=replaceStr;return infoObj;}
tmt.validator.filters.ltrim=tmt.validator.createFilter("^(\\s*)(\\b[\\w\\W]*)$","$2");tmt.validator.filters.rtrim=tmt.validator.createFilter("^([\\w\\W]*)(\\b\\s*)$","$1");tmt.validator.filters.nospaces=tmt.validator.createFilter("\\s*","");tmt.validator.filters.nocommas=tmt.validator.createFilter(",","");tmt.validator.filters.nodots=tmt.validator.createFilter("\\.","");tmt.validator.filters.noquotes=tmt.validator.createFilter("'","");tmt.validator.filters.nodoublequotes=tmt.validator.createFilter('"',"");tmt.validator.filters.nohtml=tmt.validator.createFilter("<[^>]*>","");tmt.validator.filters.alphanumericonly=tmt.validator.createFilter("[^\\w]","");tmt.validator.filters.numbersonly=tmt.validator.createFilter("[^\\d]","");tmt.validator.filters.lettersonly=tmt.validator.createFilter("[^a-zA-Z]","");tmt.validator.filters.commastodots=tmt.validator.createFilter(",",".");tmt.validator.filters.dotstocommas=tmt.validator.createFilter("\\.",",");tmt.validator.filters.numberscommas=tmt.validator.createFilter("[^\\d,]","");tmt.validator.filters.numbersdots=tmt.validator.createFilter("[^\\d\\.]","");tmt.validator.filterField=function(fieldNode){var filtersArray=fieldNode.getAttribute("tmt:filters").split(",");if(window.event){var code=window.event.keyCode;if((code==37)||(code==38)||(code==39)||(code==40)){return;}}
for(var i=0;i<filtersArray.length;i++){var filtObj=tmt.validator.filters[filtersArray[i]];if(filtObj){fieldNode.value=fieldNode.value.replace(filtObj.rex,filtObj.str)}
if(filtersArray[i]=="demoronizer"){fieldNode.value=tmt.form.stringDemoronizer(fieldNode.value);}}}
tmt.validator.dateStrToObj=function(dateStr,datePattern){var globalObj=tmt.validator.datePatterns[datePattern];if(globalObj){var dateBits=dateStr.split(globalObj.s);var testDate=new Date(dateBits[globalObj.y],(dateBits[globalObj.m]-1),dateBits[globalObj.d]);var isDate=(testDate.getFullYear()==dateBits[globalObj.y])&&(testDate.getMonth()==dateBits[globalObj.m]-1)&&(testDate.getDate()==dateBits[globalObj.d]);if(isDate&&globalObj.rex.test(dateStr)){return testDate;}
return null;}
return null;}
tmt.validator.getCallback=function(formNode){if(formNode.getAttribute("tmt:callback")){return formNode.getAttribute("tmt:callback");}
return tmt.validator.DEFAULT_CALLBACK;}
tmt.validator.defaultCallback=function(formNode,validators){var errorMsg="";var focusGiven=false;for(var i=0;i<validators.length;i++){errorMsg+=validators[i].message+"\n";if(!focusGiven&&(validators[i].getFocus)){validators[i].getFocus();focusGiven=true;}}
if(errorMsg!=""){alert(errorMsg);}}
tmt.validator.errorBoxCallback=function(formNode,validators){if(validators.length==0){tmt.form.removeDisplayBox(formNode);return;}
var focusGiven=false;var htmlStr="<ul>";for(var i=0;i<validators.length;i++){htmlStr+="<li><em>"+validators[i].name+": </em> "+validators[i].message+"</li>";if(!focusGiven&&(validators[i].getFocus)){validators[i].getFocus();focusGiven=true;}}
htmlStr+="</ul>";tmt.form.displayErrorMessage(formNode,htmlStr);}
tmt.validator.multiSectionDefaultCallback=function(formNode,hasErrors,sectionResults){var errorMsg="";for(var i=0;i<sectionResults.length;i++){if(sectionResults[i].validators.length==0){continue;}
var validators=sectionResults[i].validators;for(var k=0;k<validators.length;k++){errorMsg+=validators[k].message+"\n";}}
if(errorMsg!=""){alert(errorMsg);}}
tmt.validator.multiSectionBoxCallback=function(formNode,hasErrors,sectionResults){if(!hasErrors){tmt.form.removeDisplayBox(formNode);return;}
var htmlStr="<ul>";for(var i=0;i<sectionResults.length;i++){if(sectionResults[i].validators.length==0){continue;}
htmlStr+="<li><strong>"+sectionResults[i].label+"</strong>";var validators=sectionResults[i].validators;htmlStr+="<ul>";for(var k=0;k<validators.length;k++){htmlStr+="<li><em>"+validators[k].name+": </em> "+validators[k].message+"</li>";}
htmlStr+="</ul></li>";}
htmlStr+="</ul>";tmt.form.displayErrorMessage(formNode,htmlStr);}
tmt.validator.defaultFieldCallback=function(fieldNode,validator){if(validator){tmt.validator.defaultCallback(fieldNode.form,[validator]);}}
tmt.validator.errorBoxFieldCallback=function(fieldNode,validator){if(validator){tmt.validator.errorBoxCallback(fieldNode.form,[validator]);}
else{tmt.validator.errorBoxCallback(fieldNode.form,[]);}}
tmt.addEvent(window,"load",tmt.validator.init);