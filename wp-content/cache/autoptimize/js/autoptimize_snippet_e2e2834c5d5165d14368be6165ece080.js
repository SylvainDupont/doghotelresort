if(typeof(tmt)=="undefined"){var tmt={};}
tmt.addEvent=function(obj,type,fn){if(obj.addEventListener){obj.addEventListener(type,fn,false);}
else if(obj.attachEvent){obj["e"+type+fn]=fn;obj[type+fn]=function(){obj["e"+type+fn](window.event);}
obj.attachEvent("on"+type,obj[type+fn]);}}
tmt.get=function(){var returnNodes=new Array();for(var i=0;i<arguments.length;i++){var nodeElem=arguments[i];if(typeof nodeElem=="string"){nodeElem=document.getElementById(nodeElem);}
if(arguments.length==1){return nodeElem;}
returnNodes.push(nodeElem);}
return returnNodes;}
tmt.getAll=function(startNode){var rootNode=(startNode)?tmt.get(startNode):document;return rootNode.getElementsByTagName("*");}
tmt.getAllNodes=function(startNode){var elements=tmt.getAll(startNode);var nodesArray=[];for(var i=0;i<elements.length;i++){if(elements[i].nodeType==1){nodesArray.push(elements[i]);}}
return nodesArray;}
tmt.getNodesByAttribute=function(attName,startNode){var nodes=tmt.getAll(startNode);return tmt.filterNodesByAttribute(attName,nodes);}
tmt.getNodesByAttributeValue=function(attName,attValue,startNode){var nodes=tmt.getAll(startNode);return tmt.filterNodesByAttributeValue(attName,attValue,nodes);}
tmt.filterNodesByAttribute=function(attName,nodes){var filteredNodes=new Array();for(var i=0;i<nodes.length;i++){if(nodes[i].getAttribute(attName)){filteredNodes.push(nodes[i]);}}
return filteredNodes;}
tmt.filterNodesByAttributeValue=function(attName,attValue,nodes){var filteredNodes=new Array();for(var i=0;i<nodes.length;i++){if(nodes[i].getAttribute(attName)&&(nodes[i].getAttribute(attName)==attValue)){filteredNodes.push(nodes[i]);}}
return filteredNodes;}
tmt.setNodeAttribute=function(nodeList,attName,attValue){for(var i=0;i<nodeList.length;i++){var nodeElem=tmt.get(nodeList[i]);if(nodeElem){nodeElem[attName]=attValue;}}}
tmt.addClass=function(element,className){var nodeElem=tmt.get(element);if(!nodeElem||(tmt.hasClass(nodeElem,className)==true)){return;}
nodeElem.className+=(nodeElem.className?" ":"")+className;}
tmt.hasClass=function(element,className){var nodeElem=tmt.get(element);if(nodeElem){return nodeElem.className.search(new RegExp("\\b"+className+"\\b"))!=-1;}
return null;}
tmt.removeClass=function(element,className){var nodeElem=tmt.get(element);if(!nodeElem||(tmt.hasClass(nodeElem,className)==false)){return;}
nodeElem.className=nodeElem.className.replace(new RegExp("\\s*\\b"+className+"\\b","g"),"");}
tmt.toggleClass=function(element,className){var nodeElem=tmt.get(element);if(tmt.hasClass(nodeElem,className)){tmt.removeClass(nodeElem,className);}
else{tmt.addClass(nodeElem,className);}}
tmt.trim=function(str){return str.replace(/^\s+|\s+$/g,"");}
tmt.encodeEntities=function(str){if(str&&str.search(/[&<>"]/)!=-1){str=str.replace(/&/g,"&amp;");str=str.replace(/</g,"&lt;");str=str.replace(/>/g,"&gt;");str=str.replace(/"/g,"&quot;");}
return str}
tmt.unencodeEntities=function(str){str=str.replace(/&amp;/g,"&");str=str.replace(/&lt;/g,"<");str=str.replace(/&gt;/g,">");str=str.replace(/&quot;/g,'"');return str}
tmt.hashToEncodeURI=function(obj){var values=[];for(var x in obj){values.push(encodeURIComponent(x)+"="+encodeURIComponent(obj[x]));}
return values.join("&");};