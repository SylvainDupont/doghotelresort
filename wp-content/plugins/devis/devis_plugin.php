<?php
/*
Plugin Name: Gestionnaire de Devis
Plugin URI: https://www.facebook.com/Krealine.fr/?ref=bookmarks
Description: Un plugin de générateur de devis
Version: 0.2
Author: DUPONT Sylvain
Author URI: https://www.facebook.com/Krealine.fr/?ref=bookmarks
License: GPL2
*/
include_once plugin_dir_path( __FILE__ ).'/devis.php';
include_once plugin_dir_path( __FILE__ ).'/devis_form.php';

class Devis_Plugin {
    public $nuitreducchien;
    public $nuitreducchat;
    
    public function __construct()
    {
        $this->nuitreducchien = 6;
        $this->nuitreducchat = 10;
        
        register_activation_hook(__FILE__, array('Devis', 'install'));
        register_uninstall_hook(__FILE__, array('Devis', 'uninstall'));
        
        add_action('admin_menu', array($this, 'add_admin_menu'));
        add_action('wp_ajax_update_holidays', array($this, 'update_holidays'));
        add_action('wp_ajax_update_ferie', array($this, 'update_ferie'));
        add_action('wp_ajax_delete_ferie', array($this, 'delete_ferie'));
        add_action('wp_ajax_delete_holiday', array($this, 'delete_holiday'));
        add_action('wp_ajax_recalcul_tarif_chiens_vs', array($this, 'recalcul_tarif_chiens_vs'));
        add_action('wp_ajax_recalcul_tarif_chiens_hvs', array($this, 'recalcul_tarif_chiens_hvs'));
        add_action('wp_ajax_recalcul_tarif_chat', array($this, 'recalcul_tarif_chat'));
        new Devis_Form();
    }
    public function update_holidays() {
        global $wpdb;
        if(isset($_POST['periode'])):
            Devis::updateHolidays($_POST['periode']);
            $rowHolidays = $wpdb->get_results("SELECT id,periode,DATE_FORMAT(DateDebut, '%d/%m/%Y') as DateDebut,DATE_FORMAT(DateFin, '%d/%m/%Y') as DateFin FROM {$wpdb->prefix}devis_vacances WHERE periode = '".$_POST['periode']."' ORDER BY DateDebut ASC");
            echo json_encode(array("success"=>true,"message"=>null,"data"=>$rowHolidays));
        else:
            echo json_encode(array("success"=>false,"message"=>"Periode non reconnue"));
        endif;
	wp_die();
    }
    public function delete_ferie() {
        global $wpdb;
        if(isset($_POST['id'])):
            $wpdb->query("DELETE FROM {$wpdb->prefix}devis_jours_feries WHERE id='".$_POST['id']."'");
            echo json_encode(array("success"=>true,"message"=>null));
        else:
            echo json_encode(array("success"=>false,"message"=>"Jour férié non reconnu"));
        endif;
	wp_die();
    }
    public function delete_holiday() {
        global $wpdb;
        if(isset($_POST['id'])):
            $wpdb->query("DELETE FROM {$wpdb->prefix}devis_vacances WHERE id='".$_POST['id']."'");
            echo json_encode(array("success"=>true,"message"=>null));
        else:
            echo json_encode(array("success"=>false,"message"=>"Période non reconnu"));
        endif;
	wp_die();
    }
    public function recalcul_tarif_chiens_vs() {
        global $wpdb;
        $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
        $res=array();
        $prixChien2Base = $row->vsjf_chien+($row->vsjf_chien*(1-($row->majoration_chien_2/100)));
        $tauxDegressif = (100-$row->taux_reduc_nuit_6)/100;
        
        for($i=1;$i<31;$i++):
            $reduction = ($row->vsjf_chien*($i-($this->nuitreducchien-1))-(($row->vsjf_chien*($i-($this->nuitreducchien-1)))*$tauxDegressif) > 0) ? $row->vsjf_chien*($i-($this->nuitreducchien-1))-(($row->vsjf_chien*($i-($this->nuitreducchien-1)))*$tauxDegressif) : 0;
            $reduction2 = ($prixChien2Base*($i-($this->nuitreducchien-1))-(($prixChien2Base*($i-($this->nuitreducchien-1)))*$tauxDegressif) > 0) ? $prixChien2Base*($i-($this->nuitreducchien-1))-(($prixChien2Base*($i-($this->nuitreducchien-1)))*$tauxDegressif) : 0;
            
            array_push($res,array(
                $i,
                $row->vsjf_chien*$i-$reduction,
                ($row->vsjf_chien*$i-$reduction)+$row->majoration_am_chien,
                $reduction,
                $prixChien2Base*$i-$reduction2,
                ($prixChien2Base*$i-$reduction2)+$row->majoration_am_chien,
                $reduction2
            ));
        endfor;
        echo json_encode(array("success"=>true,"message"=>null,"data"=>$res));
	wp_die();
    }
    public function recalcul_tarif_chiens_hvs() {
        global $wpdb;
        $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
        $res=array();
        $prixChien2Base = $row->hvs_chien+($row->hvs_chien*(1-($row->majoration_chien_2/100)));
        $tauxDegressif = (100-$row->taux_reduc_nuit_6)/100;
        
        for($i=1;$i<31;$i++):
            $reduction = ($row->hvs_chien*($i-($this->nuitreducchien-1))-(($row->hvs_chien*($i-($this->nuitreducchien-1)))*$tauxDegressif) > 0) ? $row->hvs_chien*($i-($this->nuitreducchien-1))-(($row->hvs_chien*($i-($this->nuitreducchien-1)))*$tauxDegressif) : 0;
            $reduction2 = ($prixChien2Base*($i-($this->nuitreducchien-1))-(($prixChien2Base*($i-($this->nuitreducchien-1)))*$tauxDegressif) > 0) ? $prixChien2Base*($i-($this->nuitreducchien-1))-(($prixChien2Base*($i-($this->nuitreducchien-1)))*$tauxDegressif) : 0;
            
            array_push($res,array(
                $i,
                $row->hvs_chien*$i-$reduction,
                ($row->hvs_chien*$i-$reduction)+$row->majoration_am_chien,
                $reduction,
                $prixChien2Base*$i-$reduction2,
                ($prixChien2Base*$i-$reduction2)+$row->majoration_am_chien,
                $reduction2
            ));
        endfor;
        echo json_encode(array("success"=>true,"message"=>null,"data"=>$res));
	wp_die();
    }
    public function recalcul_tarif_chat() {
        global $wpdb;
        $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
        $res=array();
        $prixChat2 = $row->vsjf_chat+($row->vsjf_chat*(1-($row->majoration_chat_2/100)));
        $prixChat3 = $prixChat2+($row->vsjf_chat*(1-($row->majoration_chat_3/100)));
        
        $tauxDegressif = $row->taux_reduc_nuit_10/100;
        
        for($i=1;$i<31;$i++):
            
            $reduction = ($i>10) ? $row->vsjf_chat*($i-10)*$tauxDegressif : 0;
            $reduction2 = ($i>10) ? $prixChat2*($i-10)*$tauxDegressif : 0;
            $reduction3 = ($i>10) ? $prixChat3*($i-10)*$tauxDegressif : 0;
        
            array_push($res,array(
                $i,
                $row->vsjf_chat*$i-$reduction,
                $prixChat2*$i-$reduction2,
                $prixChat3*$i-$reduction3
            ));
        endfor;
        echo json_encode(array("success"=>true,"message"=>null,"data"=>$res));
	wp_die();
    }
    public function update_ferie() {
        global $wpdb;
        if(isset($_POST['year'])):
            $wpdb->query("DELETE FROM {$wpdb->prefix}devis_jours_feries WHERE year='".$_POST['year']."'");
            $timestamp = Devis::getJoursFeries($_POST['year'],true);
            foreach($timestamp as $k=>$t):
			// var_dump($timespamp);
                $wpdb->query("INSERT INTO {$wpdb->prefix}devis_jours_feries (`year`,`timestamp`,`nom`,`etat`) VALUES (".$_POST['year'].",".(array_keys($timestamp[$k])[0]).",'".$t[array_keys($timestamp[$k])[0]]."',1)");
            endforeach;
            $rowFeries = $wpdb->get_results("SELECT id,timestamp,nom FROM {$wpdb->prefix}devis_jours_feries WHERE year = '".$_POST['year']."' ORDER BY timestamp");
            echo json_encode(array("success"=>true,"message"=>null,"data"=>$rowFeries));
        else:
            echo json_encode(array("success"=>false,"message"=>"Periode non reconnue"));
        endif;
	wp_die();
    }
    public function add_admin_menu()
    {
        add_menu_page('Demande de Devis', 'Devis', 'manage_options', 'devis', array($this, 'add_menu'));
        add_submenu_page('devis', 'Devis', 'Paramètres', 'manage_options', 'devis_plugin', array('Devis', 'devis_parametre'));
        // add_submenu_page('devis', 'Devis', 'Vacances et jours fériés', 'manage_options', 'devis_holidays', array('Devis', 'devis_holidays'));
    }
    public function add_menu()
    {
        global $wpdb;
        if(isset($_GET['delete'])):
                $wpdb->query("DELETE FROM {$wpdb->prefix}devis_saisie WHERE id=".$_GET['delete']);
                $file = dirname(__FILE__).'/../../../temp/devis_'.$_GET['delete'].'.pdf';
                @unlink($file);
        endif;
        echo '<h1>Devis - Liste des demandes</h1>';
        wp_enqueue_script( 'devis-plugin-js', plugins_url( '/js/devis_plugin.js', __FILE__ ));

        $row = $wpdb->get_results("SELECT * FROM {$wpdb->prefix}devis_saisie ORDER BY date_demande DESC");
        echo "<div class='card' style='width:95%;max-width:95%;float:left'>";
            echo "<table style='width:95%;border-collapse:collapse'>";
                echo "<thead>";
                    echo "<tr>";
                        echo "<th>N°</th><th style='text-align:right'>Type</th><th style='text-align:right'>Nombre</th><th>Période</th><th>du</th><th>au</th><th>Options</th><th style='text-align:right'>Total</th><th style='text-align:right'>Action</th>";
                    echo "</tr>";
                echo "</thead>";
                echo "<tbody>";
                    foreach($row as $r):
                        echo "<tr>";
                            echo "<td>";
                                echo $r->id;
                            echo "</td>";
                            echo "<td style='text-align:right'>";
                                    echo $r->type_resident;
                            echo "</td>";
                            echo "<td style='text-align:right'>";
                                    echo $r->nbr_resident;
                            echo "</td>";
                            echo "<td style='text-align:center'>";
                                    if($r->type_periode==1):
                                            echo "Hors vacances scolaires";
                                    elseif($r->type_periode==1):
                                            echo "Vacances scolaires et jours fériés";
                                    else:
                                            echo "Vacances d'été";
                                    endif;
                            echo "</td>";
                            echo "<td style='text-align:center'>";
                                    $ddeb = new \DateTime($r->date_debut);
                                    echo $ddeb->format('d/m/Y \à H:i');
                            echo "</td>";
                            echo "<td style='text-align:center'>";
                                    $dfin = new \DateTime($r->date_fin);
                                    echo $dfin->format('d/m/Y \à H:i');
                            echo "</td>";
                            echo "<td>";
                                    echo "<ul style='list-style-type:none'>";
                                            if($r->opt_webcam==1):
                                                    echo "<li>> Webcam</li>";
                                            endif;
                                            if($r->opt_television==1):
                                                    echo "<li>> Télévision</li>";
                                            endif;
                                            if($r->opt_traitement==1):
                                                    echo "<li>> Traitement médicament</li>";
                                            endif;
                                            if($r->opt_brossage==1):
                                                    echo "<li>> Brossage, nettoyage des oreilles, ...</li>";
                                            endif;
                                    echo "</ul>";
                            echo "</td>";
                            echo "<td style='text-align:right'>";
                                    echo $r->TotalPRIX." €";
                            echo "</td>";
                            echo "<td style='text-align:right'>";
                                    echo "<a href='../devis/?print=".$r->id."' target='_blank' title='Voir le devis' class='dashicons-before dashicons-search' style='text-decoration:none'></a>";
                                    echo "<a href='?page=devis&delete=".$r->id."' title='Supprimer le devis' class='dashicons-before dashicons-no' style='color:red !important; text-decoration:none'></a>";
                            echo "</td>";
                        echo "</tr>";
                    endforeach;
                echo "</tbody>";
            echo "</table>";
        echo "</div>";
    }
}

new Devis_Plugin();
