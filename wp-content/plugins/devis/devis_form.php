<?php
require_once dirname(__FILE__).'/html2pdf/vendor/autoload.php';
require_once dirname(__FILE__).'/functions.php';
use Spipu\Html2Pdf\Html2Pdf;
use Spipu\Html2Pdf\Exception\Html2PdfException;
use Spipu\Html2Pdf\Exception\ExceptionFormatter;
class Devis_Form{    public $nuitreducchien;    public $nuitreducchat;    public function __construct()    {        $this->nuitreducchien = 6;        $this->nuitreducchat = 10;        add_shortcode('devis_form_saisie', array($this, 'form_html'));    }     public function form_html($atts, $content)    {         global $wpdb;
        $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");       		echo "<script type='text/javascript'>var basePlugin='".plugin_dir_url( __FILE__ )."';</script>";        		wp_enqueue_style( 'devis-parametre-css', plugins_url( '/css/devis_form_front.css', __FILE__ ));       		wp_enqueue_style( 'devis-parametre-css-datetimepicker', plugins_url( '/css/jquery.datetimepicker.min.css', __FILE__ ));   		wp_enqueue_script( 'devis-parametre-js-datetimepicker', plugins_url( '/js/jquery.datetimepicker.full.min.js', __FILE__ ),array(),false,true);    		wp_enqueue_script( 'devis-parametre-js', plugins_url( '/js/devis_form_front.js', __FILE__ ),array(),false,true);        		if(!isset($_GET['print'])):            		if(!isset($_POST['form_devis_submit'])):                		$html = render_form($row,$_GET);                                            		echo "<div class='formDevis'>".$html."</div>";            		else:                		$Ddebut = explode(" ",$_POST['date_debut']);                		$DdebutDate = explode("/",$Ddebut[0]);                		$DateDebut = $DdebutDate[1].'/'.$DdebutDate[0].'/'.$DdebutDate[2].' '.$Ddebut[1];                		$Dfin = explode(" ",$_POST['date_fin']);                		$DfinDate = explode("/",$Dfin[0]);               		$DateFin = $DfinDate[1].'/'.$DfinDate[0].'/'.$DfinDate[2].' '.$Dfin[1];      		$DateFinCalculMidi = $DfinDate[1].'/'.$DfinDate[0].'/'.$DfinDate[2].' 12:00:00';          		$dateDebut = new \DateTime($DateDebut);              		$dateFin = new \DateTime($DateFin);     		$dateFinCalculMidi = new \DateTime($DateFinCalculMidi);      		$interval = date_diff($dateDebut, $dateFin);          		$isAM = $dateFinCalculMidi < $dateFin;		$hf  = explode(":",$Dfin[1]);							$hd  = explode(":",$Ddebut[1]);			$majorj = 0;						if($hf[0]>12 && $hd[0]>12 && $hf[0]<$hd[0]):									$majorj++;							endif;
                if($_POST['type_resident']=="chien"):									
                    $tarifs = $this->calcul_tarif_chiens($dateDebut,$interval->days+$majorj,$isAM,$_POST);                				else:                    					$tarifs = $this->calcul_tarif_chats($dateDebut,$interval->days+$majorj,$isAM,$_POST);                				endif;
                echo "<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
                    echo "<div class='wpb_column vc_column_container vc_col-sm-12'>";
                        echo "<div class='resultDevis'>";
                            echo "<div class='wpb_column vc_column_container vc_col-sm-12'>";
                            echo "<h1>Votre devis* : </h1>";
                                    echo "<p>Vous avez demandé une réservation pour ".$_POST['nbr_resident'].' '.ucfirst($_POST['type_resident']).(($_POST['nbr_resident']>1) ? 's':'')."</p>";
                                    echo "<p><strong>Date d'arrivée demandée : </strong>".$dateDebut->format('d/m/Y')." à ".$dateDebut->format("H:i")."</p>";
                                    echo "<p><strong>Date de départ demandée : </strong>".$dateFin->format('d/m/Y')." à ".$dateFin->format("H:i")."</p>";


                                    if(isset($_POST['webcam']) || isset($_POST['television']) || isset($_POST['medicament']) || isset($_POST['brossage'])):
                                            echo "<p><strong>Options demandées :</strong></p>";
                                            echo "<ul>";
                                                    if(isset($_POST['webcam'])):
                                                            echo "<li>Webcam</li>";
                                                    endif;
                                                    if(isset($_POST['television'])):
                                                            echo "<li>Télévision</li>";
                                                    endif;
                                                    if(isset($_POST['medicament'])):
                                                            echo "<li>Traitement médicament</li>";
                                                    endif;
                                                    if(isset($_POST['brossage'])):
                                                            echo "<li>Brossage, nettoyage des oreilles, ...</li>";
                                                    endif;
                                            echo "</ul>";
                                    endif;

                                    echo "<p><strong>Nombre de jour facturé : </strong>".$tarifs['nbNuits']."</p>";
                                    echo "<hr />";
                            echo "</div>";
                    echo "</div>";

                    echo "Soit un prix total de <strong>". $tarifs['prixFinal']." € </strong><br /><br />";                    
		    echo "Un accompte de <strong>". $tarifs['accompte']." € </strong> vous sera demandé";			echo "<hr />";			echo "<i>« Ce devis n’a pas valeur de réservation. <br >Une réservation est effective après la visite de l’établissement. »</i>"; 
                    

                    $optsLink = array();
                    if(isset($_POST['webcam'])):
                            array_push($optsLink,1);
                    else:
                            array_push($optsLink,0);
                    endif;
                    if(isset($_POST['television'])):
                            array_push($optsLink,1);
                    else:
                            array_push($optsLink,0);
                    endif;
                    if(isset($_POST['medicament'])):
                            array_push($optsLink,1);
                    else:
                            array_push($optsLink,0);
                    endif;
                    if(isset($_POST['brossage'])):
                            array_push($optsLink,1);
                    else:
                            array_push($optsLink,0);
                    endif;
                    $req="INSERT INTO {$wpdb->prefix}devis_saisie(type_resident,nbr_resident,type_periode,date_debut,date_fin,opt_webcam,opt_television,
                    opt_traitement,opt_brossage,date_demande,IDSettings,TotalPRIX) VALUES ('".ucfirst($_POST['type_resident'])."',".$_POST['nbr_resident'].",0,'".$dateDebut->format('Y-m-d H:i:s')."','".$dateFin->format('Y-m-d H:i:s')."',".implode(',',$optsLink).",NOW(),'".$row->id."',".$tarifs['prixFinal'].");";
                    
                    $wpdb->query($req);

                    $lastRow = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_saisie ORDER BY id DESC LIMIT 0,1");

                    echo "<p><center><a role='button' href='?qtt=".$_POST['nbr_resident']."&typeR=".$_POST['type_resident']."&ddeb=".$_POST['date_debut']."&dfin=".$_POST['date_fin']."&opts=".implode(',',$optsLink)."' class='btn btn-success' style='color:white !important'>Modifier le devis</a>";
                    
                    echo "<a role='button' class='btn btn-success' style='margin-left:5px;color:white !important' href='?print=".$lastRow->id."'>Imprimer le devis</a></center></p>";
				
                    if($row->email_contact!==""):
                        $header = array('From: contact@doghotelresort.com','Content-Type: text/html; charset=UTF-8');
                        $result = wp_mail($row->email_contact,"Nouveau devis","Un nouveau devis vient d'être généré depuis votre site<br /><a target='_blank' href='http://abso-prod.eu/doghotelresort/devis/?print=".$lastRow->id."'>Cliquez-ici pour le visualiser</a>", $header);
                    endif;
            endif;
    else:
            Devis_Form::printME($_GET['print']);
    endif;
    }
    
    
    
    
	public static function printME($idRow){
		global $wpdb;
		$dataSaisie = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_saisie WHERE id = ".$idRow);
		
		if($dataSaisie!=null):
			$dataParam = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings WHERE id = ".$dataSaisie->IDSettings);
			$html="<table style='width:100%;margin-top:25px'>";
				$html.="<tr>";
					$html.="<td style='width:170px;padding-left:20px;'>";
						$html.="<img src=".dirname(__FILE__)."/../../uploads/2018/01/Dog-Hotel-Resort-Complexe-RVB.png' style='max-height:150px;max-width:150px'>";
					$html.="</td>";
					$html.="<td style='padding-left:0px;padding-top:10px;vertical-align:top;font-size:18px;line-height:26px;'>";
						$html.="<strong>DOG HOTEL RESORT</strong><br />Z.A Les Sables de Sary<br />53 allée Georges Charpak<br />45770 SARAN<br />Tél. : 09 83 32 10 55";
					$html.="</td>";
				$html.="</tr>";
			$html.="</table>";
			$html.="<table style='width:100%;margin-top:25px'>";
				$html.="<tr>";
					$html.="<td style='width:100%;text-align:center;'>";
						$html.="<h1>DEVIS N°".$idRow."</h1>";
					$html.="</td>";
				$html.="</tr>";
			$html.="</table>";
			
			$demandeDate = new \DateTime($dataSaisie->date_demande);
			$html.="<p style='margin:40px 0 20px 0;font-size:16px;font-weight:700;text-align:right;padding-right:50px;'>Le <strong> ".$demandeDate->format('d/m/Y')."</strong></p>";
			$html.="<p style='margin:20px 0 20px 0;font-size:16px;font-weight:700'>Vous avez demandé une réservation pour ".$dataSaisie->nbr_resident.' '.ucfirst($dataSaisie->type_resident).(($dataSaisie->nbr_resident>1) ? 's':'')."</p>";
			
			$ddebut = new \DateTime($dataSaisie->date_debut);
			$dfin = new \DateTime($dataSaisie->date_fin);
			$interval = date_diff($ddebut, $dfin);
				
			if( $interval->invert == 1 ):
				$ddebut = new \DateTime($dataSaisie->date_fin);
				$dfin = new \DateTime($dataSaisie->date_debut);
				$interval = date_diff($ddebut, $dfin);
			endif;
			$nbDays = $interval->format("%a");
			
			$optsLink = array();
			if($dataSaisie->opt_webcam==1 || $dataSaisie->opt_television==1 || $dataSaisie->opt_traitement== 1 ||  $dataSaisie->opt_brossage== 1):
				$htmlOPTS= "<p><strong>Options demandées : </strong></p><ul style='list-style-type:none;text-align:left;margin:0;padding:0;max-width:200px;'>";
					if($dataSaisie->opt_webcam==1):
						array_push($optsLink,1);
						$htmlOPTS.= "<li style='margin:0;padding:0'>> Webcam</li>";
					else:
						array_push($optsLink,0);
					endif;
					if($dataSaisie->opt_television==1):
						array_push($optsLink,1);
						$htmlOPTS.= "<li style='margin:0;padding:0'>> Télévision</li>";
					else:
						array_push($optsLink,0);
					endif;
					if($dataSaisie->opt_traitement==1):
						array_push($optsLink,1);
						$htmlOPTS.= "<li style='margin:0;padding:0'>> Traitement médicament</li>";
					else:
						array_push($optsLink,0);
					endif;
					if($dataSaisie->opt_brossage==1):
						array_push($optsLink,1);
						$htmlOPTS.= "<li style='margin:0;padding:0'>> Brossage, nettoyage des oreilles, ...</li>";
					else:
						array_push($optsLink,0);
					endif;
				$htmlOPTS.= "</ul>";
			else:
				$optsLink = array(0,0,0,0);
				$htmlOPTS="";
			endif;
			
			$html.= '<p style="margin-bottom:0"><strong>Dates : </strong></p>';
			$html.= '<p style="padding-left:15px;">';
			$html.= 'Du '.$ddebut->format('d/m/Y \à H:i').'<br />';
			$html.= 'au '.$dfin->format('d/m/Y \à H:i').'</p>';
			$html.= '<p>Soit un total facturé de : <strong>'.$nbDays.' jour(s)</strong></p>';
			// $html.= '<p><strong>Période : </strong> : '.(($dataSaisie->type_periode==1) ? "Hors vacances scolaires" : (($dataSaisie->type_periode==2) ? "Vacances scolaires et jours fériés" : "Vacances d'été")).'</p>';
			$html.= $htmlOPTS;
			
			$html.= '<hr />';
			$html.= '<p><strong>TOTAL : '.round((($dataSaisie->TotalPRIX*100)/100),2).' €</strong></p>';
			

			$html2pdf = new Html2Pdf('P', 'A4', 'fr');
			$html2pdf->pdf->SetDisplayMode('fullpage');
			$html2pdf->writeHTML($html);
			$file = dirname(__FILE__).'/../../../temp/devis_'.$dataSaisie->id.'.pdf';
			$fileURL = $_SERVER['PHP_SELF'].'/temp/devis_'.$dataSaisie->id.'.pdf';
			$html2pdf->output($file, 'F');
			
			echo "<div style='margin-bottom:100px;'><iframe style='width:100%;min-height:800px;' src='".$fileURL."'></iframe><br />";
			echo "<p><center><a role='button' href='?qtt=".$dataSaisie->nbr_resident."&typeR=".(($dataSaisie->type_resident=='Chien') ? 'chien' : 'chat')."&typeP=".$dataSaisie->type_periode."&ddeb=".$ddebut->format('Y-m-d')."&dfin=".$dfin->format('Y-m-d')."&hdeb=".$ddebut->format('H:i')."&hfin=".$dfin->format('H:i')."&opts=".implode(',',$optsLink)."' class='btn btn-success' style='color:white !important'>Modifier le devis</a>";
				
			echo "</div>";
			
		else:
			echo "<div class='alert alert-danger'>Une erreur est survenue</div>";
		endif;
	}
	
public function calcul_tarif_chiens($dateDebut,$limit,$isAM,$options) {
    global $wpdb;
    $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
    $prix = 0;
    $arrayDetail=array();
    for($i=1;$i<=$limit;$i++):
        $d=$dateDebut->add(new DateInterval('P'.$i.'D'));
        if($d->format('m')=="07" || $d->format('m')=="08"):
            $tarifChien = ($options['nbr_resident']==1) ? $row->vsjf_chien : $row->vsjf_chien + $row->vsjf_chien*(1-($row->majoration_chien_2/100));
            $prix+=($i<6) ? $tarifChien : $tarifChien*(1-($row->taux_reduc_nuit_6/100));
            $arrayDetail["Nuit n°".$i]=array(
                "Date"=>$d->format("d/m/Y"),
                "Vacances"=>"Oui",
                "prix unitaire"=>($i<6) ? $tarifChien : $tarifChien*(1-($row->taux_reduc_nuit_6/100))
            );
        else:
            
            $tarifChien = ($options['nbr_resident']==1) ? $row->hvs_chien : $row->hvs_chien + $row->hvs_chien*(1-($row->majoration_chien_2/100));
            $prix+=($i<6) ? $tarifChien : $tarifChien*(1-($row->taux_reduc_nuit_6/100));
            $arrayDetail["Nuit n°".$i]=array(
                "Date"=>$d->format("d/m/Y"),
                "Vacances"=>"Non",
                "prix unitaire"=>($i<6) ? $tarifChien : $tarifChien*(1-($row->taux_reduc_nuit_6/100))
            );
        endif;
        $dateDebut->sub(new DateInterval('P'.$i.'D'));
    endfor;
    $optionsParJour=$soinParJour=0;		$prixHebergement = ($isAM) ? $prix+$row->majoration_am_chat : $prix;	
    if(isset($options['webcam']) || isset($options['television'])):
        $optionsParJour = (isset($options['webcam']) && isset($options['television'])) ? $row->prix_television + $row->prix_webcam : ( (isset($options['television'])) ? $row->prix_television : $row->prix_webcam);
        $prix+=($optionsParJour*$limit);
    endif;
    if(isset($options['medicament']) || isset($options['brossage'])):
        $soinParJour = (isset($options['medicament']) && isset($options['brossage'])) ? $options['nbr_resident']*($row->prix_medicament + $row->prix_brossage) : ( (isset($options['medicament'])) ? $options['nbr_resident']*$row->prix_medicament : $options['nbr_resident']*$row->prix_brossage);
        $prix+=($soinParJour*$limit);
    endif;	
    return array(
        "nbNuits"=>$limit,
        "prixFinal"=>($isAM) ? $prix+$row->majoration_am_chien : $prix,
        "options"=>$optionsParJour,
        "soins"=>$soinParJour,		        "prixHebergement"=>$prixHebergement,		        "accompte"=>($isAM) ? ($prix+$row->majoration_am_chien)* (1-($row->taux_accompte/100)) : $prix* (1-($row->taux_accompte/100)),
        "detail"=>$arrayDetail);
}
public function calcul_tarif_chats($dateDebut,$limit,$isAM,$options) {
    global $wpdb;
    $row = $wpdb->get_row("SELECT * FROM {$wpdb->prefix}devis_settings ORDER BY DateAjout DESC LIMIT 0,1");
   
    $prix = 0;
    $arrayDetail=array();
    for($i=1;$i<=$limit;$i++):
        $d=$dateDebut->add(new DateInterval('P'.$i.'D'));
        
        if($options['nbr_resident']==1):
            $tarifChat = $row->hvs_chat;
        elseif($options['nbr_resident']==2): 
            $tarifChat = $row->hvs_chat + $row->hvs_chat*(1-($row->majoration_chat_2/100));
        elseif($options['nbr_resident']==3): 
            $tarifChat = $row->hvs_chat + $row->hvs_chat*(1-($row->majoration_chat_2/100)) + $row->hvs_chat*(1-($row->majoration_chat_3/100));
        endif;
        
        $prix+=($i<11) ? $tarifChat : $tarifChat*(1-($row->taux_reduc_nuit_10/100));
        
        $arrayDetail["Nuit n°".$i]=array(
            "Date"=>$d->format("d/m/Y"),
            "prix unitaire"=>($i<11) ? $tarifChat : $tarifChat*(1-($row->taux_reduc_nuit_6/100))
        );
        $dateDebut->sub(new DateInterval('P'.$i.'D'));
    endfor;		$prixHebergement = ($isAM) ? $prix+$row->majoration_am_chat : $prix;	
    $optionsParJour=$soinParJour=0;
    if(isset($options['webcam']) || isset($options['television'])):
        $optionsParJour = (isset($options['webcam']) && isset($options['television'])) ? $row->prix_television + $row->prix_webcam : ( (isset($options['television'])) ? $row->prix_television : $row->prix_webcam);
        $prix+=($optionsParJour*$limit);
    endif;
    if(isset($options['medicament']) || isset($options['brossage'])):
        $soinParJour = (isset($options['medicament']) && isset($options['brossage'])) ? $options['nbr_resident']*($row->prix_medicament + $row->prix_brossage) : ( (isset($options['medicament'])) ? $options['nbr_resident']*$row->prix_medicament : $options['nbr_resident']*$row->prix_brossage);
        $prix+=($soinParJour*$limit);
    endif;
    return array(
        "nbNuits"=>$limit,
        "prixFinal"=>($isAM) ? $prix+$row->majoration_am_chat : $prix,
        "options"=>$optionsParJour,		        "prixHebergement"=>$prixHebergement,                "accompte"=>($isAM) ? ($prix+$row->majoration_am_chat)* (1-($row->taux_accompte/100)) : $prix* (1-($row->taux_accompte/100)),
        "soins"=>$soinParJour,
        "detail"=>$arrayDetail);
    }
}