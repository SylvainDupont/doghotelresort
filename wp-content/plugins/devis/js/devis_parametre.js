jQuery('.dashicons-update-right-blue').on("click",function(){
   jQuery(this).addClass("rotating");
   var periode = jQuery("#tbl-holiday").attr("data-attr-id");
   jQuery("#tbl-holiday tbody tr").remove();
   
   jQuery.ajax({
        url: ajaxurl,
        method: "POST",
        dataType:"json",
        data: { 'action': 'update_holidays',periode: periode}
   }).done(function(data) {
        if(data.success){
            for(var e in data.data){
                jQuery('<tr id="'+data.data[e].id+'"><td>'+data.data[e].periode+'</td><td>'+data.data[e].DateDebut+'</td><td>'+((data.data[e].DateFin=="00/00/0000") ? " ? " : data.data[e].DateFin)+'</td><td><button class="btn btn-danger btn-delete-holiday"><span class="dashicons dashicons-no"></span></button></td></tr>').appendTo("#tbl-holiday tbody");
            }
            jQuery('.dashicons-update-right-blue').removeClass("rotating");
            jQuery('.btn-delete-holiday').off("click");
            jQuery('.btn-delete-holiday').on("click",function(){
                var idToDel=jQuery(this).parent().parent().attr("id");
                deleteholiday(idToDel);
            });
        }
        else{
            alert("Une erreur est survenue");
        }
   });
});
jQuery('.dashicons-update-right-blue-ferie').on("click",function(){
   jQuery(this).addClass("rotating");
   var y = jQuery("#tbl-ferie").attr("data-attr-id");
   jQuery("#tbl-ferie tbody tr").remove();
   jQuery.ajax({
        url: ajaxurl,
        method: "POST",
        dataType:"json",
        data: { 'action': 'update_ferie',year: y}
   }).done(function(data) {
        if(data.success){
            for(var e in data.data){
                var d = new Date(data.data[e].timestamp*1000);
                jQuery('<tr id="'+data.data[e].id+'"><td>'+((d.getDate()<10) ? '0'+d.getDate() : d.getDate())+'/'+(((d.getMonth()+1)<10) ? '0'+(d.getMonth()+1) : d.getMonth()+1)+'/'+d.getFullYear()+'</td><td>'+data.data[e].nom+'</td><td><button class="btn btn-danger btn-delete-ferie" title="Supprimer ce jour férié"><span class="dashicons dashicons-no"></span></button></td></tr>').appendTo("#tbl-ferie tbody");
            }
            jQuery('.dashicons-update-right-blue-ferie').removeClass("rotating");
            jQuery('.btn-delete-ferie').off("click");
            jQuery('.btn-delete-ferie').on("click",function(){
                var idToDel=jQuery(this).parent().parent().attr("id");
                deleteFerie(idToDel);
            });
        }
        else{
            alert("Une erreur est survenue");
        }
   });
});

jQuery('form[name=submityear] select').on("change",function(){
   jQuery('form[name=submityear]').submit();
});

jQuery('.btn-delete-ferie').on("click",function(){
   var idToDel=jQuery(this).parent().parent().attr("id");
   deleteFerie(idToDel);
});

function deleteFerie(idToDel){
    if(confirm("Êtes-vous sûr de vouloir supprimer ce jour férié ?")){
       jQuery('.dashicons-update-right-blue-ferie').addClass("rotating");
       jQuery.ajax({
            url: ajaxurl,
            method: "POST",
            dataType:"json",
            data: { 'action': 'delete_ferie',id: idToDel}
       }).done(function(data) {
            if(data.success){
                jQuery("table#tbl-ferie tbody tr#"+idToDel).remove();
                jQuery('.dashicons-update-right-blue-ferie').removeClass("rotating");
            }
            else{
                alert("Une erreur est survenue");
            }
       });
   }
}
jQuery('.btn-delete-holiday').on("click",function(){
   var idToDel=jQuery(this).parent().parent().attr("id");
   deleteholiday(idToDel);
});

function deleteholiday(idToDel){
    if(confirm("Êtes-vous sûr de vouloir supprimer cette période ?")){
       jQuery('.dashicons-update-right-blue').addClass("rotating");
       jQuery.ajax({
            url: ajaxurl,
            method: "POST",
            dataType:"json",
            data: { 'action': 'delete_holiday',id: idToDel}
       }).done(function(data) {
            if(data.success){
                jQuery("table#tbl-holiday tbody tr#"+idToDel).remove();
                jQuery('.dashicons-update-right-blue').removeClass("rotating");
            }
            else{
                alert("Une erreur est survenue");
            }
       });
   }
}

jQuery(document).ready(function(){
    
   var tabs = jQuery( "#tabs" ).tabs();
   recalculchiensvs();
   tabs.on( "click", function() {
      var panelId = tabs.find( ".ui-tabs-active" ).find("a").attr("href");
      if(panelId=="#tabs-1"){
          recalculchiensvs();
      }
      else if(panelId=="#tabs-2"){
          recalculchienshvs();
      }
      else if(panelId=="#tabs-3"){
          recalculchat();
      }
   });
});

function recalculchiensvs(){
    jQuery("table#simulationchienvs tbody tr").remove();
    jQuery.ajax({
        url: ajaxurl,
        method: "POST",
        dataType:"json",
        data: { 'action': 'recalcul_tarif_chiens_vs'}
    }).done(function(data) {
        if(data.success){
            for(var e in data.data){
                var tr = "<tr>";
                tr +="<td>"+data.data[e][0]+"</td>";
                tr +="<td>"+data.data[e][1]+" €</td>";
                tr +="<td>"+data.data[e][2]+" €</td>";
                tr +="<td>"+data.data[e][3]+" €</td>";
                tr +="<td>"+data.data[e][4]+" €</td>";
                tr +="<td>"+data.data[e][5]+" €</td>";
                tr +="<td>"+data.data[e][6]+" €</td>";
                tr += "</tr>";
                jQuery(tr).appendTo("table#simulationchienvs tbody");
            }
        }
        else{
            alert("Une erreur est survenue");
        }
    });
}
function recalculchienshvs(){
    jQuery("table#simulationchienhvs tbody tr").remove();
    jQuery.ajax({
        url: ajaxurl,
        method: "POST",
        dataType:"json",
        data: { 'action': 'recalcul_tarif_chiens_hvs'}
    }).done(function(data) {
        if(data.success){
            for(var e in data.data){
                var tr = "<tr>";
                tr +="<td>"+data.data[e][0]+"</td>";
                tr +="<td>"+data.data[e][1]+" €</td>";
                tr +="<td>"+data.data[e][2]+" €</td>";
                tr +="<td>"+data.data[e][3]+" €</td>";
                tr +="<td>"+data.data[e][4]+" €</td>";
                tr +="<td>"+data.data[e][5]+" €</td>";
                tr +="<td>"+data.data[e][6]+" €</td>";
                tr += "</tr>";
                jQuery(tr).appendTo("table#simulationchienhvs tbody");
            }
        }
        else{
            alert("Une erreur est survenue");
        }
    });
}
function recalculchat(){
    jQuery("table#simulationchat tbody tr").remove();
    jQuery.ajax({
        url: ajaxurl,
        method: "POST",
        dataType:"json",
        data: { 'action': 'recalcul_tarif_chat'}
    }).done(function(data) {
        if(data.success){
            for(var e in data.data){
                var tr = "<tr>";
                tr +="<td>"+data.data[e][0]+"</td>";
                tr +="<td>"+data.data[e][1]+" €</td>";
                tr +="<td>"+data.data[e][2]+" €</td>";
                tr +="<td>"+data.data[e][3]+" €</td>";
                tr += "</tr>";
                jQuery(tr).appendTo("table#simulationchat tbody");
            }
        }
        else{
            alert("Une erreur est survenue");
        }
    });
}