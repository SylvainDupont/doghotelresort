<?php
function render_form($row,$get){
    $html="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
            $html.="<div class='wpb_column vc_column_container vc_col-sm-12'>";
                    $html.="<div class='columns1_2'>
                            <p class='center'>Tarif dégressif de -".$row->taux_reduc_nuit_6."% dès la 6e nuit pour les chiens.</p>
                            <p class='center'>Tarif dégressif de -".$row->taux_reduc_nuit_10."% dès la 10e nuit pour les chats.</p>
                            <p class='center'>Second chien / chat qui partage une chambre -50%.</p>
                    </div>";

                    $html.="<div class='columns1_2' style='text-align:center'>
                                    ".$row->txt_question." 
                            </div>";
            $html.="</div>";
    $html.="</div>";
    $html.="<hr /><br />";
    $html.="<form method='POST' action='#' name='form_devis'>";
        $html.="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
                $html.="<div class='wpb_column vc_column_container vc_col-sm-6 centercontent'>";

                        $html.="<div class='bloc-input'><label for='type_resident'>Type de résident :</label>";
                        $html.='<div class="label-input">';
                            if(isset($get['typeR'])):
                                 $html.='<label class="radio-inline">
                                    <input type="radio" '.(($get['typeR']=="chien") ? "checked='checked'" : "").' name="type_resident" id="type_resident1" value="chien" required="required"> Chien(s)
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" '.(($get['typeR']=="chat") ? "checked='checked'" : "").' name="type_resident" id="type_residen2" value="chat" required="required"> Chat(s)
                                </label>';
                            else:
                                 $html.='<label class="radio-inline">
                                    <input type="radio" checked="checked" name="type_resident" id="type_resident1" value="chien" required="required"> Chien(s)
                                </label>
                                <label class="radio-inline">
                                    <input type="radio" name="type_resident" id="type_residen2" value="chat" required="required"> Chat(s)
                                </label>';
                            endif;
                            
                        $html.="</div>";
						$html.="</div>";
				$html.="</div>";
                $html.="<div class='wpb_column vc_column_container vc_col-sm-6 centercontent' >";
                        $html.="<div class='bloc-input'><label for='nbr_resident'>Nombre de résident par chambre :</label>";
                        $html.='<div class="label-input">';
                            if(isset($get['qtt'])):
                                $html.='<label class="radio-inline"><input type="radio" name="nbr_resident" id="nbr_resident1" '.(($get['qtt']=="1") ? "checked='checked'" : "").' value="1" required="required"> 1</label>
                                <label class="radio-inline"><input type="radio" name="nbr_resident" id="nbr_resident2" '.(($get['qtt']=="2") ? "checked='checked'" : "").' value="2" required="required"> 2</label>
                                <label class="radio-inline"><input type="radio" name="nbr_resident" id="nbr_resident3" '.(($get['qtt']=="3") ? "checked='checked'" : "").' value="3" required="required"> 3</label>';
                            else:
                                $html.='<label class="radio-inline"><input type="radio" name="nbr_resident" id="nbr_resident1" checked="checked" value="1" required="required"> 1</label>
                                <label class="radio-inline"><input type="radio" name="nbr_resident" id="nbr_resident2" value="2" required="required"> 2</label>
                                <label class="radio-inline" style="display:none"><input type="radio" name="nbr_resident" id="nbr_resident3" value="3" required="required"> 3</label>';
                          
                            endif;
                        $html.='</div>';   
						$html.="</div>";
				$html.="</div>"; 
        $html.="</div>"; 
        $html.="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
                $html.="<div class='wpb_column vc_column_container vc_col-sm-12 centercontent' style='margin-top:30px;margin-bottom:30px'>";
                        $html.="<label>Période de séjour : </label> ".$row->txt_periode;
                $html.="</div>";
        $html.="</div>";

        $html.="<div class=''>";
                $html.="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";	
                        $html.="<div class='wpb_column vc_column_container vc_col-sm-6 centercontent' >";
                                $dateDeb = new \DateTime(date('Y').'-'.date('m').'-'.date('d'));
                                $html.="<label for='date_debut'>Date et heure d'arrivée :</label> <input class='form-control' style='margin-bottom:15px' type='text' name='date_debut' id='date_debut' required='required'>";

                        $html.="</div>";				
                        $html.="<div class='wpb_column vc_column_container vc_col-sm-6 centercontent'>";
                                $dateFin = new \DateTime(date('Y').'-'.date('m').'-'.date('d'));
                                $dateFin->add(new \DateInterval('P1D'));
                                $html.="<label for='date_fin'>Date et heure de départ : </label><input class='form-control' type='text' name='date_fin' id='date_fin' required='required'> ";

                        $html.="</div>";
                $html.="</div>";		
        $html.="</div>";
    $opts = (isset($get['opts'])) ? explode(",",$get['opts']) : array(0,0,0,0);
        $html.="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";	
            $html.="<div class='wpb_column vc_column_container vc_col-sm-12 centercontent' style='margin-top:30px'>";
                    $html.="<br /><label>Vous pouvez également sélectionner différentes options :</label><br />";
                     $html.='<div class="label-input"><label class="radio-inline">
                                            <input type="checkbox" ' .(($opts[0]==1) ? "checked='checked'" : "").' name="webcam" id="webcam" value="5"> Webcam / jour (5€)
                                    </label>
                                    <label class="radio-inline">
                                            <input type="checkbox" ' .(($opts[1]==1) ? "checked='checked'" : "").'name="television" id="television" value="5"> Télévision / jour (5€)
                                    </label>
                                    <label class="radio-inline">
                                            <input type="checkbox" ' .(($opts[2]==1) ? "checked='checked'" : "").'name="medicament" id="medicament" value="5"> Traitement médicament (5€)
                                    </label>
                                    <label class="radio-inline">
                                            <input type="checkbox" ' .(($opts[3]==1) ? "checked='checked'" : "").'name="brossage" id="brossage" value="5"> Brossage, nettoyage des oreilles, ... (5€)
                                    </label>
                        </div>
                    ';     
                 $html.="</div>";
            $html.="</div>";
        $html.="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
            $html.="<div class='wpb_column vc_column_container vc_col-sm-12' style='margin-top:30px'>";
                $html.="<br /><br />";
                $html.="<p><center><button type='submit' class='btn btn-success' name='form_devis_submit'>Calculer</button><button type='reset' class='btn btn-danger'>Réinitialiser</button></center></p>";
            $html.="</div>";
        $html.="</div>";
		$html.="<div class='wpb_raw_code wpb_content_element wpb_raw_html'>";
            $html.="<div class='wpb_column vc_column_container vc_col-sm-12' style='margin-top:30px'>";
                $html.="<p><center>Pour toute question, contactez - nous au <span style='color: #82a737;'><strong>09 83 32 10 55</strong></span></center></p>";
            $html.="</div>";
        $html.="</div>";
    $html.="</form>";
    return $html;
}

    ?>